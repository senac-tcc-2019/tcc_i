<h1> Above - Web App de Organização Pessoal com Astrologia</h1>


[![Wiki: home](https://img.shields.io/badge/wiki-home-blue.svg)](https://gitlab.com/senac-tcc-2019/tcc_i/wikis/home)
[![Issues: open](https://img.shields.io/badge/issues-open-yellow.svg)](https://gitlab.com/senac-tcc-2019/tcc_i/issues)
[![Milestones: open](https://img.shields.io/badge/milestones-open-green.svg)](https://gitlab.com/senac-tcc-2019/tcc_i/-/milestones/4)
[![Boards: TCC II - Seminário I](https://img.shields.io/badge/boards-TCC%20II%20--%20Semin%C3%A1rio%20I-yellowgreen)](https://gitlab.com/senac-tcc-2019/tcc_i/-/boards/1322711?milestone_title=TCC%20II%20-%20Semin%C3%A1rio%20I&)


-------

<a href="#aparencia">Aparência</a> &bull;
<a href="#mot">O app</a> &bull;
<a href="#tecnologias">Tecnologias</a>



<h2 id="aparencia">Aparência</h2>

![Aparência](https://gitlab.com/senac-tcc-2019/tcc_i/raw/master/aparencia-pequena.png)

<h2 id="mot">O app</h2>

O presente projeto tem o objetivo de criar uma ferramenta para a organização das tarefas cotidianas de forma lúdica e pretende ser uma aplicação web responsiva com as características de uma [PWA](https://developers.google.com/web/fundamentals/codelabs/your-first-pwapp/?hl=pt-br) (_Progressive Web App_).
 
 Apresenta uma área de anotações para organização pessoal, com calendário (que conterá informações astrológicas) e poderá ser utilizado para centralizar informações relativas a humor.

 Notificará o usuário, com _push notifications_, avisando sobre mudanças de alguns trânsitos astrológicos e mostrará, em área específica, informações sobre os seus significados segundo a Astrologia.
 
 **Abaixo estão as credenciais e endereço de acesso ao CRUD do sistema**:
 
 **Link:** [http://aboove.herokuapp.com/](http://aboove.herokuapp.com/)

**Usuário:** _admin@gmail.com_

**Senha:** 1senhadessasbicho

<h2 id="tecnologias">Tecnologias</h2>

<h3>[PWA](https://developers.google.com/web/fundamentals/codelabs/your-first-pwapp/?hl=pt-br) <i> (Progressive Web App)</i></h3>

Progressive Web Apps são experiências que combinam o melhor da Web e o melhor dos aplicativos. Trata-se uma aplicação web que possui navegação e interações muito semelhantes às de aplicativos nativos.

Precisa atender às seguintes características sendo:

* **Progressivo** - Funciona para qualquer usuário, independentemente do navegador escolhido, pois é criado com aprimoramento progressivo como princípio fundamental.
* **Responsivo** - Se adequa a qualquer formato: desktop, celular, tablet ou o que for inventado a seguir.
* **Independente de conectividade** - Aprimorado com service workers para trabalhar off-line ou em redes de baixa qualidade.
* **Semelhante a aplicativos** - Parece com aplicativos para os usuários, com interações e navegação de estilo de aplicativos, pois é compilado no modelo de shell de aplicativo.
* **Atual** - Sempre atualizado graças ao processo de atualização do service worker.
* **Seguro** - Fornecido via HTTPS para evitar invasões e garantir que o conteúdo não seja adulterado.
* **Descobrível** - Pode ser identificado como "aplicativo" graças aos manifestos W3C e ao escopo de registro do service worker, que permitem que os mecanismos de pesquisa os encontrem.
* **Reenvolvente** - Facilita o reengajamento com recursos como notificações push.
* **Instalável** - Permite que os usuários "guardem" os aplicativos mais úteis em suas telas iniciais sem precisar acessar uma loja de aplicativos.
* **Linkável** - Compartilhe facilmente por URL, não requer instalação complexa.

<h3>Framework PHP [Laravel](https://laravel.com/docs/5.8/installation)</h3>

Será utilizado para criar a API do projeto.

<h4>Requisitos:</h4>

- **Servidor**
  - PHP >= 7.1.3
  - OpenSSL PHP Extension
  - PDO PHP Extension
  - Mbstring PHP Extension
  - Tokenizer PHP Extension
  - XML PHP Extension
  - Ctype PHP Extension
  - JSON PHP Extension
  - BCMath PHP Extension


- **Composer**<br>
  O Laravel utiliza o gerenciador de dependências [Composer](https://getcomposer.org/),<br>
  por isso, tenha o Composer instalado na sua máquina antes de instalar o Laravel.
  
- **Laravel**<br>
 Faça o download do instalador do Laravel utilizando o Composer:<br>
 
   ``` composer global require "laravel/installer" ```

   
   Depois de instalar o Laravel crie um novo projeto com o comando:
 
   ``` laravel new [nome-do-projeto] ```
   
   Demais informações sobre o framework estão na [documentação do Laravel](https://laravel.com/docs/5.8).<br>

   <h3>[React](https://reactjs.org/docs/create-a-new-react-app.html) - Biblioteca <i>JavaScript</i> para Construção de Interfaces de Usuário</h3>

   <h4>Requisitos:</h4>

    - [Node](https://nodejs.org/en/docs/) >= 6 (Node.js é uma plataforma construída sobre o motor JavaScript do Google Chrome para construir aplicações de rede rápidas e escaláveis. )
    - [npm](https://www.npmjs.com/)  >= 5.2 (Gerenciador de pacotes para a linguagem de programação _JavaScript_.)

   **Para criar um projeto, use os comandos:**

   ``` 
   npx create-react-app my-app
   cd my-app
   npm start
   ```
